| Project | Progress |
| ------- | -------- |
| chess | working on book |
| backgammon | done |
| pacman | done |
| rider | partially done |
| tetris | done |
| color switch | almost done |
| don't touch the spikes | done |
| damka | done |
| fireboy and watergirl | done |
| mario | done |
| jetpack joyride | done(1/2) |
| subway surfs | done(1/2) |
| icy towers | done |
| bbtan | done |
| crossy road | done |
